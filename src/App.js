import React, {useEffect, useState} from "react";
import Task from "./Components/Task/Task";
import {useDispatch, useSelector} from "react-redux";
import {addTasks, deleteTasks, fetchTasks} from "./store/actions";

const App = () => {
    const dispatch = useDispatch();
    const tasksText = useSelector(state => state);

    useEffect(() => {
        dispatch(fetchTasks());
    }, [dispatch]);

    const [task, setTask] = useState({
        text: ''
    });

    const onInputTextareaChange = e => {
        const {name, value} = e.target;
        setTask(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const addTask = () => {
        dispatch(addTasks(task.text));
    };
    const deleteTask = (id) => {
        dispatch(deleteTasks(id));
    };

    let taskComponents;
    taskComponents = tasksText.tasks ? Object.entries(tasksText.tasks).map((task) => (
            <Task
                key={task[0]}
                text={task[1].text}
                id={task[0]}
                onDelete={() => deleteTask(task[0])}
            />
        )
    ) : <p style={{textAlign: 'center'}}>Добавьте задачу</p>

    let taskText = task ? task.text : '';
    return (
        <div>
            <div>
                <form className='form'>
                    <h3>To-do list</h3>
                    <div>
                        <input type="text" className="inp" onChange={onInputTextareaChange} value={taskText}
                               name="text"/>
                        <button className="btn" type="button" onClick={addTask}>Add task</button>
                    </div>
                </form>
            </div>
            {taskComponents}
        </div>
    )
};

export default App;
