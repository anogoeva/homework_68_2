import React from 'react';
import './Task.css'

const Task = (props) => {
    return (
        <div className='task'>
            <ul>
                <li className="list" key={props.id}>{props.text}
                </li>
                <button className="btnDel" onClick={props.onDelete}>Delete</button>
            </ul>
        </div>
    );
};
export default Task;