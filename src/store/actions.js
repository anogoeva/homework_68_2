import axios from 'axios';

export const DELETE = 'DELETE';
export const ADD = 'ADD';

export const FETCH_TASKS_REQUEST = 'FETCH_TASKS_REQUEST';
export const FETCH_TASKS_SUCCESS = 'FETCH_TASKS_SUCCESS';
export const FETCH_TASKS_FAILURE = 'FETCH_TASKS_FAILURE';

export const SAVE_COUNTER_REQUEST = 'SAVE_COUNTER_REQUEST';
export const SAVE_COUNTER_SUCCESS = 'SAVE_COUNTER_SUCCESS';
export const SAVE_COUNTER_FAILURE = 'SAVE_COUNTER_FAILURE';

export const DELETE_TASKS_REQUEST = 'DELETE_TASKS_REQUEST';
export const DELETE_TASKS_SUCCESS = 'DELETE_TASKS_SUCCESS';
export const DELETE_TASKS_FAILURE = 'DELETE_TASKS_FAILURE';

export const decrease = () => ({type: DELETE});
export const add = value => ({type: ADD, payload: value});

export const fetchTasksRequest = () => ({type: FETCH_TASKS_REQUEST});
export const fetchTasksSuccess = (tasks) => ({type: FETCH_TASKS_SUCCESS, payload: tasks});
export const fetchTasksFailure = () => ({type: FETCH_TASKS_FAILURE});

export const saveCounterRequest = () => ({type: SAVE_COUNTER_REQUEST});
export const saveCounterSuccess = (tasks) => ({type: SAVE_COUNTER_SUCCESS, payload: tasks});
export const saveCounterFailure = () => ({type: SAVE_COUNTER_FAILURE});

export const deleteTaskRequest = () => ({type: DELETE_TASKS_REQUEST});
export const deleteTaskSuccess = () => ({type: DELETE_TASKS_SUCCESS});
export const deleteTaskFailure = () => ({type: DELETE_TASKS_FAILURE});

const URL = 'https://general-bf34d-default-rtdb.firebaseio.com/tasks';

export const fetchTasks = () => {
    return async (dispatch, getState) => {
        dispatch(fetchTasksRequest());
        try {
            const response = await axios.get(URL + '.json');
            if (response.data) {

                dispatch(fetchTasksSuccess(response.data));
            } else {
                dispatch(fetchTasksSuccess(response.data));
            }
        } catch (e) {
            dispatch(fetchTasksFailure());
        }
    };
};

export const addTasks = (text) => {
    return async (dispatch) => {
        dispatch(saveCounterRequest());
        try {
            const taskText = {text: text};
            const response = await axios.post(URL + '.json', taskText);

            if (response.data) {
                dispatch(saveCounterSuccess(response.data));
            }
            window.location.href = '/';
        } catch (e) {
            dispatch(saveCounterFailure());
        }
    };
};

export const deleteTasks = (id) => {
    return async (dispatch) => {
        dispatch(deleteTaskRequest());
        try {
            const response = await axios.delete(URL + '/' + id + '.json');
            if (response.data) {
                dispatch(deleteTaskSuccess(response.data));
            }
            window.location.href = '/';
        } catch (e) {
            dispatch(deleteTaskFailure());
        }
    };
};
