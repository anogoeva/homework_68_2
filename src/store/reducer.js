import {
    ADD,
    DELETE, DELETE_TASKS_FAILURE, DELETE_TASKS_REQUEST, DELETE_TASKS_SUCCESS,
    FETCH_TASKS_FAILURE,
    FETCH_TASKS_REQUEST,
    FETCH_TASKS_SUCCESS,
    SAVE_COUNTER_FAILURE, SAVE_COUNTER_REQUEST, SAVE_COUNTER_SUCCESS,

} from "./actions";

const initialState = {
    tasks: '',
    loading: false,
    error: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case DELETE:
            return {...state, counter: state.counter - 1};
        case ADD:
            return {...state, tasks: action.payload};
        case FETCH_TASKS_REQUEST:
            return {...state, error: null, loading: true};
        case FETCH_TASKS_SUCCESS:
            return {...state, loading: false, tasks: action.payload};
        case FETCH_TASKS_FAILURE:
            return {...state, loading: false, error: action.payload};
        case SAVE_COUNTER_REQUEST:
            return {...state, error: null, loading: true};
        case SAVE_COUNTER_SUCCESS:
            return {...state, loading: false, tasks: state.tasks};
        case SAVE_COUNTER_FAILURE:
            return {...state, loading: false};
        case DELETE_TASKS_REQUEST:
            return {...state, error: null, loading: true};
        case DELETE_TASKS_SUCCESS:
            return {...state, loading: false, tasks: state.tasks};
        case DELETE_TASKS_FAILURE:
            return {...state, loading: false};
        default:
            return state;
    }
};

export default reducer;